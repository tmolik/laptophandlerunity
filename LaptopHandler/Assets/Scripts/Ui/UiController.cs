﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour
{
    public static UiController Get { get; private set; }

    public UiController()
    {
        Get = this;
    }


    public RectTransform SoundPanel;
    public RectTransform ShutdownPanel;
    public ConfigUiController ConfigPanel;
    public ConnectionUiController ConnectionPanel;

    public Button SoundButton;
    public Button ShutdownButton;
    public Button ConfigButton;

    public Color ActiveButtonColor;
    public Color NotActiveButtonColor;

    public void OnClickSoundButton()
    {
        SwitchView(EUiViewType.Sound);
    }

    public void OnClickShutdownPanel()
    {
        SwitchView(EUiViewType.Shutdown);
    }

    public void OnClickImagesPanel()
    {
        SwitchView(EUiViewType.Images);
    }

    public void OnClickConfigPanel()
    {
        SwitchView(EUiViewType.Config);
    }

    public void OpenConnectionPanel()
    {
        SwitchView(EUiViewType.Connection);
    }


    private void SwitchView(EUiViewType viewType)
    {
        SoundPanel.gameObject.SetActive(viewType == EUiViewType.Sound);
        ShutdownPanel.gameObject.SetActive(viewType == EUiViewType.Shutdown);
        ConfigPanel.gameObject.SetActive(viewType == EUiViewType.Config);
        ConnectionPanel.SwitchVisibility(viewType == EUiViewType.Connection);

        SoundButton.image.color = viewType == EUiViewType.Sound ? ActiveButtonColor : NotActiveButtonColor;
        ConfigButton.image.color = viewType == EUiViewType.Config ? ActiveButtonColor : NotActiveButtonColor;
        ShutdownButton.image.color = viewType == EUiViewType.Shutdown ? ActiveButtonColor : NotActiveButtonColor;
    }

    public void SwitchButtonsInteractable(bool interactable)
    {
        SoundButton.interactable = interactable;
        ShutdownButton.interactable = interactable;
        ConfigButton.interactable = interactable;
    }
}

