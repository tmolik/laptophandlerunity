﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UiShutdownValueButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    internal bool IsPressed = false;

    public float TimeToInvokeAction = 0.25f;
    public UnityEvent ActionOnHold;

    private float pressedTimer = 0;

    public void Update()
    {
        if (IsPressed)
        {
            pressedTimer += Time.deltaTime;
            if (pressedTimer >= TimeToInvokeAction)
            {
                ActionOnHold.Invoke();
                pressedTimer -= TimeToInvokeAction;
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        IsPressed = true;
        pressedTimer = 0;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        IsPressed = false;
    }


}
