﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ConnectionUiController : MonoBehaviour
{
    public TMP_InputField ServerIpInputField;

    public CanvasGroup CanvasGroup;

    public TextMeshProUGUI InfoLabel;

    public Button ConnectButton;

    private EConnectionStatus currentConnectionStatus = EConnectionStatus.NotConnnected;

    private bool connectionStatusChanged;



    public void Start()
    {
        ServerIpInputField.onValidateInput = ValidateInput;
    }

    public void SwitchVisibility(bool visible)
    {
        CanvasGroup.interactable = visible;
        CanvasGroup.blocksRaycasts = visible;
        CanvasGroup.alpha = visible ? 1 : 0;
    }


    public void Update()
    {
        if (connectionStatusChanged)
        {
            connectionStatusChanged = false;
            switch (currentConnectionStatus)
            {
                case EConnectionStatus.NotConnnected:
                    OnDisconnected();
                    break;
                case EConnectionStatus.Connected:
                    OnConnected();
                    break;
                case EConnectionStatus.ConnectionError:
                    OnConnectionError();
                    break;
                default:
                    break;
            }
        }
    }

    public void ChangeConnectionStatus(EConnectionStatus connectionStatus)
    {
        currentConnectionStatus = connectionStatus;
        connectionStatusChanged = true;
    }


    public void OnClickedConnectButton()
    {
        ConfigController.CurrentConfig.ServerIP = ServerIpInputField.text;
        ConfigController.SaveConfig();

        if (!TcpController.Connected)
        {
            TcpController.Get.ConnectToTcpServer();
        }

        ServerIpInputField.interactable = false;
        ConnectButton.interactable = false;
    }

    public void SetCurrentServerIpText()
    {
        ServerIpInputField.text = ConfigController.CurrentConfig.ServerIP;
    }

    static char ValidateInput(string text, int charIndex, char addedChar)
    {
        if (char.IsDigit(addedChar) || addedChar == '.')
            return addedChar;
        else
            return '\0';
    }

    public void OnConnected()
    {
        InfoLabel.text = "Succesfully connected to " + ConfigController.CurrentConfig.ServerIP;
        UiController.Get.SwitchButtonsInteractable(true);
        //SwitchVisibility(false);
    }

    public void OnConnectionError()
    {
        Debug.Log("Dzieje się on connection error");
        InfoLabel.text = "Couldn't connect to " + ConfigController.CurrentConfig.ServerIP + ". \n Check if given IP Address is correct and if there is server application running on this device.";
        ServerIpInputField.interactable = true;
        ConnectButton.interactable = true;
        SwitchVisibility(true);
        UiController.Get.SwitchButtonsInteractable(false);
    }

    public void OnDisconnected()
    {
        ServerIpInputField.interactable = true;
        ConnectButton.interactable = true;
        SwitchVisibility(true);

        UiController.Get.SwitchButtonsInteractable(false);
    }
}

public enum EConnectionStatus
{
    NotConnnected,
    Connected,
    ConnectionError
}
