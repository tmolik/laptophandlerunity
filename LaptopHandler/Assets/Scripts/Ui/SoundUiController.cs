﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundUiController : MonoBehaviour
{
    public Image MuteButtonImage;
    public Sprite MuteOnSprite;
    public Sprite MuteOffSprite;

    public Slider VolumeSlider;

    private bool setInitialVolume = false;

    public void Awake()
    {
        ApplicationEvents.OnVolumeSliderPointerUp += ChangedVolume;
    }

    public void OnDestroy()
    {
        ApplicationEvents.OnVolumeSliderPointerUp -= ChangedVolume;
    }

    public void OnClickPreviousSong()
    {
        TcpController.Get.SendMessageToServer(EMessageType.PlayPreviousSong, null);
    }

    public void OnClickNextSong()
    {
        TcpController.Get.SendMessageToServer(EMessageType.PlayNextSong, null);
    }

    public void OnClickPlayPause()
    {
        TcpController.Get.SendMessageToServer(EMessageType.PlayPause, null);
    }

    public void OnClickMute()
    {
        TcpController.Get.SendMessageToServer(EMessageType.MuteSound, null);
    }

    public void ChangedVolume()
    {
        int volume = (int)VolumeSlider.value;
        TcpController.Get.SendMessageToServer(EMessageType.ChangeSoundVolume, new string[] { volume.ToString() });
    }

    public void SetMuteSprite(bool muted)
    {
        MuteButtonImage.sprite = muted ? MuteOnSprite : MuteOffSprite;
    }

    public void Update()
    {
        if (TcpController.Get.IncomingMessagesHandler.ReceivedVolume != -1 && !setInitialVolume)
        {
            VolumeSlider.value = TcpController.Get.IncomingMessagesHandler.ReceivedVolume;
            setInitialVolume = true;
        }

        if (TcpController.Get.IncomingMessagesHandler.ReceivedNewIsMuted)
        {
            TcpController.Get.IncomingMessagesHandler.ReceivedNewIsMuted = false;
            SetMuteSprite(TcpController.Get.IncomingMessagesHandler.ReceivedIsMutedValue);
        }
    }
}
