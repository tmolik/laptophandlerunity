﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ConfigController : MonoBehaviour
{

    public static Config CurrentConfig;

    public const string FileName = "Config.json";

    public static string FilePath { get { return Application.persistentDataPath + "/" + FileName; } }


    public static void SaveConfig()
    {
        if (CurrentConfig == null)
        {
            LoadConfig();
        }
        StreamWriter file = new StreamWriter(FilePath, false);
        file.Write(JsonUtility.ToJson(CurrentConfig));
        file.Close();
    }

    public static void LoadConfig()
    {
        try
        {
            string jsonSettings = File.ReadAllText(FilePath);
            if (string.IsNullOrEmpty(jsonSettings))
            {
                Debug.Log("File was empty or nulled");
                CurrentConfig = new Config();
            }
            else
            {
                Config configFromFile = JsonUtility.FromJson<Config>(jsonSettings);
                CurrentConfig = configFromFile;
                Debug.Log("Succesfully loaded config from file");
            }
        }
        catch (Exception e)
        {
            CurrentConfig = new Config();
            Debug.Log("Something went wrong when reading file : " + e.Message);
            return;
        }

    }

}
