﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShutdownButtonsController : MonoBehaviour
{
    public TextMeshProUGUI HoursValueText;
    public TextMeshProUGUI MinutesValueText;
    public TextMeshProUGUI SecondsValueText;

    private int hoursValue;
    private int minutesValue;
    private int secondsValue;

    public void Start()
    {
        hoursValue = 0;
        minutesValue = 0;
        secondsValue = 0;
        HoursValueText.text = hoursValue.ToString();
        MinutesValueText.text = minutesValue.ToString();
        SecondsValueText.text = secondsValue.ToString();
    }

    public void OnClickAbortShutdown()
    {
        TcpController.Get.SendMessageToServer(EMessageType.AbortShutdown, null);
    }

    public void OnClickShutdown()
    {
        int seconds = hoursValue * 3600 + minutesValue * 60 + secondsValue;
        TcpController.Get.SendMessageToServer(EMessageType.Shutdown, new string[] { seconds.ToString() });
    }

    public void OnClickHoursUp()
    {
        hoursValue++;
        HoursValueText.text = hoursValue.ToString();
    }

    public void OnClickHoursDown()
    {
        hoursValue = Mathf.Max(hoursValue-1,0);
        HoursValueText.text = hoursValue.ToString();
    }

    public void OnClickMinutesUp()
    {
        minutesValue++;
        if (minutesValue == 60)
            minutesValue = 0;
        MinutesValueText.text = minutesValue.ToString();
    }

    public void OnClickMinutesDown()
    {
        minutesValue--;
        if (minutesValue == -1)
            minutesValue = 59;
        MinutesValueText.text = minutesValue.ToString();
    }

    public void OnClickSecondsUp()
    {
        secondsValue++;
        if (secondsValue == 60)
            secondsValue = 0;
        SecondsValueText.text = secondsValue.ToString();
    }

    public void OnClickSecondsDown()
    {
        secondsValue--;
        if (secondsValue == -1)
            secondsValue = 59;
        SecondsValueText.text = secondsValue.ToString();
    }
}
