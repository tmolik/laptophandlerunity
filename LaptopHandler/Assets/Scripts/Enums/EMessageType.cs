﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EMessageType
{
    MuteSound,
    ChangeSoundVolume,
    CurrentSoundVolume,
    ClientDisconnected,
    Shutdown,
    AbortShutdown,
    PlayPreviousSong,
    PlayNextSong,
    PlayPause,
    IsSoundMuted,
}
