﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EUiViewType
{
    Connection,
    Sound,
    Shutdown,
    Images,
    Config
}
