﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessagesHandler : MonoBehaviour
{

    internal int ReceivedVolume = -1;
    internal bool ReceivedIsMutedValue = false;
    internal bool ReceivedNewIsMuted = false;

    /// <summary>
    /// Messages will come in that way
    /// (int)EMessageType|value1|value2
    /// </summary>
    /// <param name="message"></param>
    public void HandleIncomingMessage(string message)
    {
        if (string.IsNullOrEmpty(message))
            return;

        string[] messageArguments = message.Split('|');

        int messageTypeInt;
        if (int.TryParse(messageArguments[0], out messageTypeInt))
        {
            EMessageType messageType = (EMessageType)messageTypeInt;

            switch (messageType)
            {
                case EMessageType.CurrentSoundVolume:
                    int volume;
                    if(int.TryParse(messageArguments[1], out volume))
                    {
                        ReceivedVolume = volume;
                        Debug.Log("Ustawiam received volume na " + volume);
                    }
                    break;
                case EMessageType.IsSoundMuted:
                    int muteValue;
                    if (int.TryParse(messageArguments[1], out muteValue))
                    {
                        ReceivedIsMutedValue = muteValue == 0 ? false : true;
                        ReceivedNewIsMuted = true;
                        Debug.Log("Ustawiam ismuted  na " + muteValue);
                    }
                    break;
                default:
                    break;
            }
        }
    }

}
