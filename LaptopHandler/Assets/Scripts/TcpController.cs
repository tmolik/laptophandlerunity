﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

public class TcpController : MonoBehaviour
{

    public static TcpController Get { get; private set; }

    public TcpController()
    {
        Get = this;
    }

    public static bool Connected;

    public MessagesHandler IncomingMessagesHandler;
    public ConnectionUiController ConnectionUi;

    private const int PORT_NO = 5000;
    private const string SERVER_IP = "192.168.0.13";

    #region private members 	
    private TcpClient socketConnection;
    private Thread clientReceiveThread;
    #endregion

    public void OnApplicationQuit()
    {
        DisconnectFromServer();
    }

    public void OnApplicationFocus(bool focus)
    {

    }

    /// <summary> 	
    /// Setup socket connection. 	
    /// </summary> 	
    internal void ConnectToTcpServer()
    {
        try
        {
            clientReceiveThread = new Thread(new ThreadStart(ListenForData));
            clientReceiveThread.IsBackground = true;
            clientReceiveThread.Start();
        }
        catch (Exception e)
        {
            Debug.Log("On client connect exception " + e);
            ConnectionUi.ChangeConnectionStatus(EConnectionStatus.ConnectionError);
            Connected = false;
        }
    }

    public void DisconnectFromServer()
    {
        SendMessageToServer(EMessageType.ClientDisconnected, null);
        if (clientReceiveThread != null)
            clientReceiveThread.Abort();
        Connected = false;
        ConnectionUi.ChangeConnectionStatus(EConnectionStatus.NotConnnected);

    }

    /// <summary> 	
    /// Runs in background clientReceiveThread; Listens for incomming data. 	
    /// </summary>     
    private void ListenForData()
    {
        try
        {
            socketConnection = new TcpClient(ConfigController.CurrentConfig.ServerIP, PORT_NO);
            if (socketConnection.Connected)
            {
                Connected = true;
                ConnectionUi.ChangeConnectionStatus(EConnectionStatus.Connected);

            }
            Byte[] bytes = new Byte[100];
            while (true)
            {
                // Get a stream object for reading 				
                using (NetworkStream stream = socketConnection.GetStream())
                {
                    int length;
                    // Read incomming stream into byte arrary. 					
                    while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        var incommingData = new byte[length];
                        Array.Copy(bytes, 0, incommingData, 0, length);
                        // Convert byte array to string message. 						
                        string serverMessage = Encoding.ASCII.GetString(incommingData);
                        IncomingMessagesHandler.HandleIncomingMessage(serverMessage);
                        Debug.Log("server message received as: " + serverMessage);
                    }
                }
            }
        }
        catch (Exception exception)
        {

            Debug.Log("Socket exception: " + exception);
            ConnectionUi.ChangeConnectionStatus(EConnectionStatus.ConnectionError);
            Connected = false;
        }
    }


    public void SendMessageToServer(EMessageType messageType, string[] arguments)
    {
        string messageToSend = ((int)messageType).ToString() + "|";
        string argumentsJoined = "";
        if (arguments != null)
            argumentsJoined = string.Join("|", arguments);
        messageToSend += argumentsJoined;

        if (socketConnection == null)
        {
            return;
        }
        try
        {
            // Get a stream object for writing. 			
            NetworkStream stream = socketConnection.GetStream();
            if (stream.CanWrite)
            {
                string clientMessage = messageToSend;
                // Convert string message to byte array.                 
                byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(clientMessage);
                // Write byte array to socketConnection stream.                 
                stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);
                Debug.Log("Client sent his message: " + messageToSend + " - should be received by server");
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("Socket exception: " + socketException);
        }
    }

    public void SendMessageToServer(byte[] byteData)
    {
        if (socketConnection == null)
        {
            return;
        }
        try
        {
            // Get a stream object for writing. 			
            NetworkStream stream = socketConnection.GetStream();
            if (stream.CanWrite)
            {
                // Write byte array to socketConnection stream.                 
                stream.Write(byteData, 0, byteData.Length);
                Debug.Log("Byte data lenght = " + byteData.Length);
                Debug.Log("Client sent his byte message:  - should be received by server");
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("Socket exception: " + socketException);
        }
    }


}
