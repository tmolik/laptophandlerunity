﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour
{

    public UiController UiController;

    public void Start()
    {
        ConfigController.LoadConfig();
        if (string.IsNullOrEmpty(ConfigController.CurrentConfig.ServerIP))
        {
            UiController.OnClickConfigPanel();
        }
        else
        {
            TcpController.Get.ConnectToTcpServer();
            UiController.ConnectionPanel.SetCurrentServerIpText();
            UiController.OpenConnectionPanel();
        }
    }

    // Update is called once per frame
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

}
